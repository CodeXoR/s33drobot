﻿using UnityEngine;
using System.Collections;

public class EFX : MonoBehaviour 
{
	[SerializeField] protected bool isAlive;
	[SerializeField] protected float animSpeed;

	public bool Alive { 
		get { return isAlive; } 
		set { 
			GetComponent<SpriteRenderer>().enabled = true;
			isAlive = value; 
			StopCoroutine("ApplyEffect");
			StartCoroutine("ApplyEffect");
		}
	} 

	public void SetPosition(Vector3 relPos)
	{
		transform.position = relPos;
	}

	public void SetOrientation(Vector3 relOrientation)
	{
		transform.right = relOrientation;
	}

	protected IEnumerator ApplyEffect()
	{
		GetComponent<Animator> ().SetBool ("Alive", isAlive);
		yield return new WaitForSeconds (animSpeed);
		isAlive = false;
		GetComponent<Animator> ().SetBool ("Alive", isAlive);
		GetComponent<SpriteRenderer>().enabled = false;
	}
}

using UnityEngine;
using System.Collections;

public class SmokePool : EFXPool
{
	static SmokePool s_smokePool;
	public static SmokePool Get() { return s_smokePool; }

	protected override void Start ()
	{
		s_smokePool = this;
		base.Start ();
	}
}
	
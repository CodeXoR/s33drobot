using UnityEngine;
using System.Collections;

public abstract class EFXPool : MonoBehaviour
{
	[SerializeField] protected EFX associateEFX;
	[SerializeField] protected EFX[] pool;
	protected int poolSize = 8;

	protected virtual void Start()
	{
		Init (poolSize);
	}

	protected void Init(int size)
	{
		pool = new EFX[size];
		for(int i = 0; i < size; i++)
		{
			EFX objEFX = Instantiate(associateEFX);
			objEFX.transform.parent = this.transform;
			pool[i] = objEFX;
		}
	}

	public EFX GetInActiveEFX()
	{
		EFX availableEFX = null;
		foreach(EFX efx in pool)
		{
			if(!efx.Alive){
				availableEFX = efx;
				break;
			}
		}
		return availableEFX;
	}
}


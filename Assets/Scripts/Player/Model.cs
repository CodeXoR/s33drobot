using UnityEngine;
using System.Collections;

public class Model : MonoBehaviour {

	[SerializeField] protected Transform head;
	[SerializeField] protected Vector3 headOriginalRotFR;
	[SerializeField] protected Vector3 headIdleRotFL;
	[SerializeField] protected Vector3 headRunRotFL;
	[SerializeField] protected Vector3 rightFace;
	[SerializeField] protected Vector3 leftFace;
	protected Animator anim;
	protected string lookAtName;
	
	public string HeadRot {
		get { return lookAtName; }
		set { lookAtName = value; }
	}

	public Animator ModelAnimator { get { return anim; } }

	protected virtual void Start()
	{
		lookAtName = "right";
		anim = GetComponent<Animator> ();
	}

	public void FaceRight()
	{
		if (transform.localEulerAngles != rightFace) {
			transform.localEulerAngles = rightFace;
		}
	}

	public void FaceLeft()
	{
		if (transform.localEulerAngles != leftFace) {
			transform.localEulerAngles = leftFace;
		}
	}

	public void AnimateJump()
	{
		S33dRobot parent = transform.parent.GetComponent<S33dRobot> ();
		EFX availableEFX = SmokePool.Get ().GetInActiveEFX ();
		availableEFX.SetPosition (parent.transform.position - (parent.Orientation * 1.1f) + (parent.transform.up * .25f));
		availableEFX.SetOrientation (-parent.Orientation);
		availableEFX.Alive = true;
	}

	public void ResetHeadOrientation(){
		if (head.transform.localEulerAngles != headOriginalRotFR) {
			head.transform.localEulerAngles = headOriginalRotFR;
		}
	}
}
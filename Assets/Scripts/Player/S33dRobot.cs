﻿using UnityEngine;
using System.Collections;

public class S33dRobot : MonoBehaviour 
{
	public bool platformOverhead;
	public bool onGround;
	[SerializeField] bool faceRight;
	[SerializeField] LayerMask groundMask;
	[SerializeField] LayerMask ceilMask;
	[SerializeField] [Range(0f, 500f)] float moveSpeed;
	[SerializeField] float jumpRate;
	[SerializeField] float firstJumpModifier = 1f;
	[SerializeField] float secondJumpModifier = 1f;
	[SerializeField] float fallGravityScale = 1f;
	[SerializeField] float normalGravityScale = 1f;
	[SerializeField] Salvador model;
	[SerializeField] int numJumps;
	Rigidbody2D body;
	Vector3 orientation;
	float ceilCheckLength;

	public Vector3 Orientation { get { return orientation; } }
	public Rigidbody2D Body { get { return body; } }
	public Salvador Model { get { return model; } }

	public bool FaceRight { 
		get { return faceRight; } 
		set { faceRight = value;}
	}
	
	void Start()
	{
		numJumps = 0;
		ceilCheckLength = 2.5f;
		body = GetComponent<Rigidbody2D> ();
		if (faceRight) {
			orientation = transform.right;
			model.FaceRight();
		}
		if (!faceRight) {
			orientation = -transform.right;
			model.FaceLeft();
		}
	}
	
	void FixedUpdate()
	{
		CheckCeiling();
		CheckGround ();
	}

	void CheckCeiling()
	{
		RaycastHit2D headHit = Physics2D.Linecast (transform.position, transform.position + transform.up * ceilCheckLength, ceilMask);
		if (headHit) {
			Vector2 vel = body.velocity;
			vel.y *= .725f;
			vel.x *= .95f;
			ceilCheckLength *= .9f;
			body.velocity = vel;
		}
	}
	
	void CheckGround()
	{
		RaycastHit2D groundHit = Physics2D.Linecast (transform.position, transform.position - transform.up * .75f, groundMask);
		CamFollow cam = Camera.main.GetComponent<CamFollow>();
		if (groundHit) {
			if(ceilCheckLength!=2.5f) { ceilCheckLength = 2.5f; }
			if(body.gravityScale!=normalGravityScale) { body.gravityScale = normalGravityScale; }
			model.ModelAnimator.SetBool("Grounded", true);
			if(numJumps != 0) { numJumps=0; }
			cam.ResetDefaults();
		}
		else
		{
			if(body.velocity.y < 0)
			{
				float tempDamping = cam.Damping;
				tempDamping-= Time.deltaTime * .35f;
				tempDamping = Mathf.Clamp(tempDamping, .1f, .8f);
				cam.Damping = tempDamping;
			}
			if(body.gravityScale!=fallGravityScale) { body.gravityScale = fallGravityScale; }
			model.ModelAnimator.SetBool("Grounded", false);
			if(numJumps == 0) { numJumps = 1; }
		}
	}

	public void Move(float displacement)
	{
		if (displacement < 0) 
		{ 
			if (orientation != -transform.right) {
				orientation = -transform.right;
			}
			if (this.faceRight) {
				this.faceRight = false;
			}
			model.FaceLeft (); 
		}

		else if (displacement > 0) 
		{ 
			if (orientation != transform.right) {
				orientation = transform.right;
			}
			if (!this.faceRight) {
				this.faceRight = true;
			}
			model.FaceRight (); 
		}

		float velocityMultipler = onGround ? displacement * 1f : displacement * 1.5f;
		model.ModelAnimator.SetFloat ("HorizontalSpeed", Mathf.Abs (displacement));
		Vector2 vel = body.velocity;
		vel.x = moveSpeed * velocityMultipler;
		vel.x = Mathf.Clamp (vel.x, -8f, 8f);
		body.velocity = vel;
	}

	public void Jump()
	{
		if (numJumps < 2)
		{
			float jumpModifier = numJumps == 0 ? firstJumpModifier : secondJumpModifier;
			Vector2 vel = body.velocity;
			vel.y += jumpRate * jumpModifier;
			vel.y = Mathf.Clamp(vel.y, -20f, 20f);
			body.velocity = vel;
			model.AnimateJump();
			numJumps++;
		}
	}
}

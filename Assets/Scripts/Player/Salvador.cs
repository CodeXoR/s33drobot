using UnityEngine;
using System.Collections;

public class Salvador : Model
{
	[SerializeField] GameObject weapon;
	int numAttacks;
	
	public void ExecuteAttack()
	{
		if(numAttacks<3)
		{
			numAttacks++;
			ModelAnimator.SetInteger("NumAttacks", numAttacks);
			
			if(numAttacks==3){
				StartCoroutine("Wait");
			}
		}
	}

	IEnumerator Wait()
	{
		yield return new WaitForSeconds (.5f);
		StopAttack();
	}

	public void StopAttack()
	{
		numAttacks = 0;
		ModelAnimator.SetInteger("NumAttacks", numAttacks);
	}

	protected override void Start ()
	{
		base.Start ();
		numAttacks = 0;
	}
}


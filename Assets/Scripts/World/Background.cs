﻿using UnityEngine;
using System.Collections;

public class Background : MonoBehaviour {

	[SerializeField] float scrollSpeed;
	float lastCamX;
	float camDeltaX;
	float lastCamY;
	float camDeltaY;

	void Start()
	{
		camDeltaX = 0f;
		lastCamX = 0f;
		lastCamY = 0f;
		camDeltaY = 0f;
	}

	void Update()
	{
		float camCurrentX = Camera.main.transform.position.x;
		float camCurrentY = Camera.main.transform.position.y;
		camDeltaX = camCurrentX - lastCamX;
		camDeltaY = camCurrentY - lastCamY;
		Vector3 newPos = transform.position;
		newPos.x -= camDeltaX;
		newPos.y -= camDeltaY;
		transform.position = Vector3.Lerp(transform.position,newPos,Time.deltaTime*scrollSpeed);
		lastCamX = Camera.main.transform.position.x;
		lastCamY = Camera.main.transform.position.y;
	}
}

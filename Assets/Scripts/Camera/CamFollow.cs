﻿using System;
using UnityEngine;


public class CamFollow : MonoBehaviour
{
	[SerializeField] float worldMinXClamp;
	[SerializeField] float worldMaxXClamp;
	[SerializeField] float worldMinYClamp;
	[SerializeField] float worldMaxYClamp;
	[SerializeField] Transform target;
	[SerializeField] float damping = 1;
	[SerializeField] float lookAheadFactor = 3;
	[SerializeField] float lookAheadReturnSpeed = 0.5f;
	[SerializeField] float lookAheadMoveThreshold = 0.1f;

	public float Damping { get { return damping; } set { damping = value; } }
	float m_OffsetZ;
	Vector3 m_LastTargetPosition;
	Vector3 m_CurrentVelocity;
	Vector3 m_LookAheadPos;

	void Start()
	{
		m_LastTargetPosition = target.position;
		m_OffsetZ = (transform.position - target.position).z;
		transform.parent = null;
	}

	void Update()
	{
		// only update lookahead pos if accelerating or changed direction
		float xMoveDelta = (target.position - m_LastTargetPosition).x;
		
		bool updateLookAheadTarget = Mathf.Abs(xMoveDelta) > lookAheadMoveThreshold;
		
		if (updateLookAheadTarget)
		{
			m_LookAheadPos = lookAheadFactor*Vector3.right*Mathf.Sign(xMoveDelta);
		}
		else
		{
			m_LookAheadPos = Vector3.MoveTowards(m_LookAheadPos, Vector3.zero, Time.deltaTime*lookAheadReturnSpeed);
		}

		Vector3 aheadTargetPos = target.position + m_LookAheadPos + Vector3.forward*m_OffsetZ;
		Vector3 newPos = Vector3.SmoothDamp(transform.position, aheadTargetPos, ref m_CurrentVelocity, damping);
		newPos.x = Mathf.Clamp(newPos.x, worldMinXClamp, worldMaxXClamp);
		newPos.y = Mathf.Clamp(newPos.y, worldMinYClamp, worldMaxYClamp);
		transform.position = newPos;
		
		m_LastTargetPosition = target.position;
	}

	public void ResetDefaults()
	{
		damping = .5f;
		lookAheadFactor = 3;
		lookAheadReturnSpeed = 0.5f;
		lookAheadMoveThreshold = 0.1f;
	}
}


﻿using UnityEngine;
using System.Collections;

public class World : MonoBehaviour {

	[SerializeField] bool[] activatedSublevels;
	Vector3 originalScale;
	Vector3 targetScale;

	public bool[] ActivatedSubLevels { get { return activatedSublevels; } }
	public Vector3 OriginalScale { get { return originalScale; } }
	public Vector3 TargetScale
	{
		get { return targetScale; }
		set {
			targetScale = value;
			if(targetScale.y > transform.localScale.y){
				StopCoroutine("ScaleBigger");
				StartCoroutine("ScaleBigger");
			}
			else{
				StopCoroutine("ScaleToOriginal");
				StartCoroutine("ScaleToOriginal");
			}
		}
	}

	void Start()
	{
		originalScale = transform.localScale;
		targetScale = originalScale;
	}

	IEnumerator ScaleBigger()
	{
		while(true)
		{
			transform.localScale = Vector3.Lerp(transform.localScale, targetScale, Time.deltaTime * 5.0f);
			float diff = Mathf.Abs (transform.localScale.y - targetScale.y);
			if(diff <= .01f){
				break;
			}
			yield return null;
		}
		transform.localScale = targetScale;
	}

	IEnumerator ScaleToOriginal()
	{
		while(true)
		{
			transform.localScale = Vector3.Lerp(transform.localScale, originalScale, Time.deltaTime * 5.0f);
			float diff = Mathf.Abs (transform.localScale.y - originalScale.y);
			if(diff <= .01f){
				break;
			}
			yield return null;
		}
		transform.localScale = originalScale;
	}
}

﻿using UnityEngine;
using System.Collections;

public class TransitionManager: MonoBehaviour 
{
	[SerializeField] string activeScreen;
	[SerializeField] GameObject model;
	[SerializeField] WorldContainer wContainer;
	[SerializeField] Vector3 modelStartOrientation;
	Animator uiAnim;
	ScreenSwipeInput input;
	
	void Start()
	{
		uiAnim = GetComponent<Animator> ();
		activeScreen = TransitionInfo.Screen.MAIN;
		input = GetComponent<ScreenSwipeInput> ();
	}

	void Update()
	{
		if (activeScreen == TransitionInfo.Screen.WORLD_SELECT) {
			float slideRate = input.DoSwipeInput (activeScreen);
			if(slideRate != 0 ) { wContainer.SlideRate = slideRate; }
		}

		else if(activeScreen == TransitionInfo.Screen.MAIN){
			input.DoSwipeInput (activeScreen, model);
		}
	}

	public void GoToWorldSelected()
	{
		string levelName = wContainer.ActiveWorld.name + wContainer.SelectedLevel.name;
		Application.LoadLevel (levelName);
	}

	public void TransitionToWorld()
	{
		uiAnim.SetBool ("GoToWorld", true);
		input.AdjustSwipeArea('w');
	}

	public void TransitionToMain()
	{
		uiAnim.SetBool ("GoToWorld", false);
		input.AdjustSwipeArea('m');
		model.transform.localEulerAngles = modelStartOrientation;
	}

	public void SetActiveScreen(string active)
	{
		if(activeScreen!=active)
			activeScreen = active;
	}
}

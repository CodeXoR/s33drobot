using UnityEngine;
using System.Collections;

public static class SwipeInputSettings
{
	public sealed class SwipeArea
	{
		public const float MAIN_MIN_X = 0f;
		public const float MAIN_MAX_X = .75f;
		public const float MAIN_MIN_Y = 0f;
		public const float MAIN_MAX_Y = .75f;
		public const float WORLD_SELECT_MIN_X = 0f;
		public const float WORLD_SELECT_MAX_X = 1f;
		public const float WORLD_SELECT_MIN_Y = .25f;
		public const float WORLD_SELECT_MAX_Y = .75f;
	}

	public sealed class SwipeSpeed
	{
		public class Rotation
		{
			public const float RATE = 2.5f;
		}

		public class Slide
		{
			public const float MIN_RATE = -4.0f;
			public const float MAX_RATE = 4.0f;
			public const float RATE_MULTIPLIER = 8.0f;
		}
	}
}


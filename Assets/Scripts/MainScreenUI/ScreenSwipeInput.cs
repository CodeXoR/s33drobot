﻿using UnityEngine;
using System.Collections;

public class ScreenSwipeInput : MonoBehaviour
{
	[SerializeField][Range(1f,8f)] float rotationSpeed;
	[SerializeField] bool inRotPadArea;
	[SerializeField] float swipeLength;
	float rotPadMinX;
	float rotPadMaxX;
	float rotPadMinY;
	float rotPadMaxY;

	void Start()
	{
		swipeLength = 0f;
		rotationSpeed = SwipeInputSettings.SwipeSpeed.Rotation.RATE;
		rotPadMinX = SwipeInputSettings.SwipeArea.MAIN_MIN_X;
		rotPadMaxX = SwipeInputSettings.SwipeArea.MAIN_MAX_X;
		rotPadMinY = SwipeInputSettings.SwipeArea.MAIN_MIN_Y;
		rotPadMaxY = SwipeInputSettings.SwipeArea.MAIN_MAX_Y;
	}

	void RotateObj(GameObject obj, float angle)
	{
		obj.transform.Rotate(Vector3.up, angle * rotationSpeed);
	}
	
	public float DoSwipeInput(string screen, GameObject obj = null)
	{
		if (Input.touchCount == 0)
			return 0f;

		switch(Input.touches[0].phase)
		{
		case TouchPhase.Began:
			Vector3 pos = Camera.main.ScreenToViewportPoint(Input.touches[0].position);
			if (pos.x >= rotPadMinX && pos.x <= rotPadMaxX &&
			    pos.y >= rotPadMinY && pos.y <= rotPadMaxY){
				inRotPadArea = true;
			}
			if(inRotPadArea && screen == TransitionInfo.Screen.WORLD_SELECT){
				swipeLength = 0f;
			}
			break;
		case TouchPhase.Moved:
			if(inRotPadArea && screen == TransitionInfo.Screen.MAIN){
				if(!obj){ break; };
				float deltaX = Input.touches[0].deltaPosition.normalized.x;
				RotateObj(obj, -deltaX);
			}
			else if(inRotPadArea && screen == TransitionInfo.Screen.WORLD_SELECT){
				swipeLength += Input.touches[0].deltaPosition.normalized.x 
							* SwipeInputSettings.SwipeSpeed.Slide.RATE_MULTIPLIER * Time.fixedDeltaTime;
			}
			break;
		case TouchPhase.Ended:
			if(inRotPadArea && screen == TransitionInfo.Screen.MAIN){
				inRotPadArea = false;
			}
			else if(inRotPadArea && screen == TransitionInfo.Screen.WORLD_SELECT){
				swipeLength = Mathf.Round(swipeLength);
				swipeLength = Mathf.Clamp(swipeLength, 
				                          SwipeInputSettings.SwipeSpeed.Slide.MIN_RATE, 
				                          SwipeInputSettings.SwipeSpeed.Slide.MAX_RATE);
				return swipeLength;
			}
			break;
		default:
			break;
		}

		return 0f;
	}

	public void AdjustSwipeArea(char flag)
	{
		if (flag == 'm') {
			rotPadMinX = SwipeInputSettings.SwipeArea.MAIN_MIN_X;
			rotPadMaxX = SwipeInputSettings.SwipeArea.MAIN_MAX_X;
			rotPadMinY = SwipeInputSettings.SwipeArea.MAIN_MIN_Y;
			rotPadMaxY = SwipeInputSettings.SwipeArea.MAIN_MAX_Y;
		}

		else if(flag == 'w'){
			rotPadMinX = SwipeInputSettings.SwipeArea.WORLD_SELECT_MIN_X;
			rotPadMaxX = SwipeInputSettings.SwipeArea.WORLD_SELECT_MAX_X;
			rotPadMinY = SwipeInputSettings.SwipeArea.WORLD_SELECT_MIN_Y;
			rotPadMaxY = SwipeInputSettings.SwipeArea.WORLD_SELECT_MAX_Y;
		}
	}
}

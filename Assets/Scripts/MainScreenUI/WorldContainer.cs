
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class WorldContainer : MonoBehaviour
{
	[SerializeField] List<World> worlds;
	[SerializeField] List<Button> activeWorldSublevels;
	[SerializeField] GameObject activeWorld;
	[SerializeField] GameObject selectedLevel;
	Coroutine activeCoroutine;
	GameObject lastActiveWorld;
	Vector3 targetPos;
	float slideRate;

	public GameObject SelectedLevel {
		get { return selectedLevel; }
		set { selectedLevel = value; }
	}

	public GameObject ActiveWorld 
	{ 
		get { return activeWorld; } 
		set { 
			activeWorld = value;
			if(activeWorld!=lastActiveWorld){
				if(lastActiveWorld){
					lastActiveWorld.GetComponent<World>().TargetScale = TransitionInfo.WorldScale.SMALL * Vector3.one;
				}
				if(activeWorld){
					activeWorld.GetComponent<World>().TargetScale = TransitionInfo.WorldScale.BIG * Vector3.one;
				}
				lastActiveWorld = activeWorld;
			}
		}
	}

	public GameObject LastActiveWorld 
	{ 
		get { return lastActiveWorld; } 
		set { lastActiveWorld = value; }
	}

	public float SlideRate
	{
		get { return slideRate; }
		set {
			slideRate = value;

			transform.localPosition = targetPos;
			targetPos = new Vector3 ((500f * slideRate) + transform.localPosition.x,0f,0f);
			targetPos.x = Mathf.Round (targetPos.x);

			if(activeWorld) { 
				activeWorld.GetComponent<World>().TargetScale = TransitionInfo.WorldScale.SMALL * Vector3.one; 
				activeWorld = null;
				lastActiveWorld = null;
			}
			if(slideRate > 0){
				if(activeCoroutine!=null) { StopCoroutine(activeCoroutine); }
				StopCoroutine("SlideRight");
				activeCoroutine = StartCoroutine("SlideRight");
			}
			else {
				if(activeCoroutine!=null) { StopCoroutine(activeCoroutine); }
				StopCoroutine("SlideLeft");
				activeCoroutine = StartCoroutine("SlideLeft");
			}
		}
	}

	void OnEnable()
	{
		ScaleWorldAtCenter ();
	}

	void OnDisable()
	{
		transform.localPosition = Vector3.zero;
		targetPos = Vector3.zero;
		this.ActiveWorld.transform.localScale = Vector3.one;
		lastActiveWorld = null;
	}

	void UpdateSublevels()
	{
		bool[] currentState = activeWorld.GetComponent<World> ().ActivatedSubLevels;
		for( int i = 0; i < currentState.Length; i++ ) {
			activeWorldSublevels[i].interactable = currentState[i];
		}
	}

	public void ScaleWorldAtCenter()
	{
		World wCenter = null;
		foreach (World world in worlds) {
			if(world.transform.position.x <= 10f && world.transform.position.x >= -10f){
				wCenter = world;
			}
		}
		this.ActiveWorld = wCenter.gameObject;
		UpdateSublevels ();
	}

	IEnumerator SlideRight()
	{
		targetPos.x = Mathf.Clamp (targetPos.x, -2000f, 2000f);
		while (true) {
			transform.localPosition = Vector3.Lerp(transform.localPosition, targetPos, Time.deltaTime * 10.0f);
			float diff = Mathf.Abs(transform.localPosition.x-targetPos.x);
			if( diff <= 1f){
				break;
			}
			yield return null;
		}
		transform.localPosition = targetPos;
		ScaleWorldAtCenter ();
	}
	
	IEnumerator SlideLeft()
	{
		targetPos.x = Mathf.Clamp (targetPos.x, -1500f, 2000f);
		while (true) {
			transform.localPosition = Vector3.Lerp(transform.localPosition, targetPos, Time.deltaTime * 10.0f);
			float diff = Mathf.Abs(transform.localPosition.x-targetPos.x);
			if( diff <= 1f){
				break;
			}
			yield return null;
		}
		transform.localPosition = targetPos;
		ScaleWorldAtCenter ();
	}
}


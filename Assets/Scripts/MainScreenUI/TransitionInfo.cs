using UnityEngine;
using System.Collections;

public static class TransitionInfo 
{
	public static class Screen
	{
		public const string MAIN = "main";
		public const string WORLD_SELECT = "worldSelect";
	}
	public static class WorldScale
	{
		public const float BIG = 1.4f;
		public const float SMALL = 1f;
	}
}


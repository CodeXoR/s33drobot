using UnityEngine;
using System.Collections;

public abstract class ATouchArea : MonoBehaviour
{
	[SerializeField]protected GameObject associateObject;
	[SerializeField]protected float areaStartX;
	[SerializeField]protected float areaEndX;
	[SerializeField]protected float areaStartY;
	[SerializeField]protected float areaEndY;
	[SerializeField]protected bool inArea;
	protected int touchFingerID; 

	public GameObject AssociateObject { get { return associateObject; } }
	public int TouchFingerID { get { return touchFingerID; } set { touchFingerID = value; } }
	public bool InArea { get { return inArea; } set { inArea = value; } }

	public bool IsInTouchArea(Vector2 touchPos)
	{
		Vector3 viewPortPos = Camera.main.ScreenToViewportPoint(touchPos);
		return (viewPortPos.x >= areaStartX && viewPortPos.x <= areaEndX &&
		        viewPortPos.y >= areaStartY && viewPortPos.y <= areaEndY);
	}

	protected void Start() { touchFingerID = -1; }

	protected abstract void LateUpdate();
	protected abstract void DoAction();
	protected abstract void ExecuteAction();
}


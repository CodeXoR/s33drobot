﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Controls : MonoBehaviour 
{
	static Controls s_controls;
	public static Controls Get() { return s_controls; }
	
	public S33dRobot player;
	public MoveTouchArea moveControlArea;
	public Button attackButton;
	float xMovement = 0f;

	void Start()
	{
		s_controls = this;
	}

	void Update()
	{
#if UNITY_STANDALONE_WIN
		float h = Input.GetAxis ("Horizontal");
		if (player.Model.ModelAnimator.GetInteger ("NumAttacks") == 0) {
			player.Move (h); 
		}
#endif
#if UNITY_ANDROID
		if (Input.touchCount > 0) {
			for (int i = 0; i < Input.touchCount; i++) {
				moveControlArea.TouchFingerID = moveControlArea.IsInTouchArea (Input.touches [i].position) ? i : moveControlArea.TouchFingerID;
			}
	
			xMovement = moveControlArea.AssociateObject.GetComponent<RectTransform> ().anchoredPosition.x / 60f; 
			if (player.Model.ModelAnimator.GetInteger("NumAttacks") == 0) {
				player.Move (xMovement);
			}
		}
		else
		{
			if(Mathf.Abs(xMovement-0f) >= 0.1f)
			{
				xMovement -= Mathf.Sign(xMovement) * Time.deltaTime * 4.0f;
			}
			else
			{
				xMovement = 0f;
			}
			Debug.Log(xMovement);
			player.Move(xMovement);
		}
#endif
	}
}

using UnityEngine;
using System.Collections;

public class MoveTouchArea : ATouchArea
{
	protected override void DoAction()
	{
		Vector2 deltaPos = associateObject.GetComponent<RectTransform>().anchoredPosition;
		deltaPos.x += Input.touches[touchFingerID].deltaPosition.x * 2.0f;
		deltaPos.y += Input.touches[touchFingerID].deltaPosition.y * 2.0f;
		deltaPos.x = Mathf.Clamp(deltaPos.x, -60f, 60f);
		deltaPos.y = Mathf.Clamp(deltaPos.y, -60f, 60f);
		associateObject.GetComponent<RectTransform>().anchoredPosition = deltaPos;
	}

	protected override void ExecuteAction ()
	{
		this.DoAction ();
	}

	protected override void LateUpdate ()
	{
		if (touchFingerID == -1)
			return;
		
		switch (Input.touches [touchFingerID].phase) 
		{
		case TouchPhase.Began:
			inArea = true;
			break;
		case TouchPhase.Moved:
			if(inArea){ ExecuteAction(); }
			break;
		case TouchPhase.Stationary:
			if(inArea){}
			break;
		case TouchPhase.Ended:
			inArea = false;
			associateObject.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
			touchFingerID = -1;
			break;
		default:
			break;
		}
	}
}

